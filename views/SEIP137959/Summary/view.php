<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Summary\Summary;


$summary = new Summary();

$summarySingle = $summary->prepare($_GET)->view();

?>
<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Summary</h1>
        </div>

        <div class="list-group">
            <div class="list-group-item">ID: <?= $summarySingle['id'] ?></div>
            <div class="list-group-item">Summary: <?= $summarySingle['summary'] ?></div>
        </div>

        <p>
            <a href="edit.php?id=<?= $summarySingle['id'] ?>" class="btn btn-primary">Edit</a>
            <a href="delete.php?id=<?= $summarySingle['id'] ?>" class="btn btn-danger">Delete</a>
        </p>

    </div>
</div>

</body>
</html>