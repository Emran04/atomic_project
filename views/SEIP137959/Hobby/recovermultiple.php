<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Hobby\Hobby;

$ids = implode(",", $_POST['mark']);
$hobby = new Hobby();

$hobby->recoverMultiple($ids);