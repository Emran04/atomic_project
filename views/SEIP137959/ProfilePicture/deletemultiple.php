<?php
require_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;

$profileO = new ImageUploader();

$profiles = $profileO->indexTrash();

foreach ($profiles as $profile) {

    if( in_array($profile['id'], $_POST['chk']) ) {
        $path = $_SERVER['DOCUMENT_ROOT'] . "/AtomicProjects/resources/images/" . $profile['image_name'];

        unlink($path);
    }

}

$ids = implode(',', $_POST['chk']);


$profileO->deleteMultiple($ids);