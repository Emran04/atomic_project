<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Book\Book;
use App\Bitm\SEIP137959\Message\Message;

$book = new Book();

if(!empty($_SESSION['message'])) {
    $message = Message::message();
}

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists("itemPerPage", $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}else {
    $_SESSION['itemPerPage'] = 5;
}


$itemsPerPage = $_SESSION['itemPerPage'];

$totalItem = $book->count();

$totalPages = ceil($totalItem / $itemsPerPage);

if(array_key_exists("pageNumber", $_GET)) {
    $pageNumber = $_GET['pageNumber'];
} else {
    $pageNumber = 1;
}
$pagination = "";

for( $i=1; $i<=$totalPages; $i++ ){
    $class = ( $pageNumber== $i)? "active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom = $itemsPerPage * ($pageNumber -1);


if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) {
    $bookList = $book->paginator($pageStartFrom,$itemsPerPage);
}
if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')) {
    $bookList = $book->prepare($_POST)->index();
}
if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&& isset($_GET['search'])) {
    $bookList = $book->prepare($_GET)->index();
}


$availableTitle=$book->getAllTitle();

$comma_separated= '"'.implode('","',$availableTitle).'"';

$availableDescription = $book->getAllDescription();

$comma_separatedDes= '"'.implode('","',$availableDescription).'"';

?>


<?php include_once '../../../includes/header.php' ?>

<div class="container-fluid">

    <?php include_once '../../../includes/sidebar.php' ?>

    <div class="col-md-8 col-md-offset-1">
        <div class="page-header">
            <h1>Books</h1>
        </div>

        <?php if(isset($message)) : ?>
            <div id="message" class="alert alert-<?php echo $message['lvl'];  ?>" role="alert">
                <?php echo $message['message'];  ?>
            </div>
        <?php endif; ?>


        <div class="row">
            <div class="col-md-6">
                <p>
                    <a href="create.php" class="btn btn-primary">Create New Book</a>
                </p>
            </div>
            <div class="col-md-6 itemsPPage">
                <form action="" method="get">
                    <label for="num">Items Per Page:</label>
                    <select class="form-control items" name="itemPerPage" id="num">
                        <option <?php if($itemsPerPage == 5) echo "selected" ?>>5</option>
                        <option <?php if($itemsPerPage == 10) echo "selected" ?>>10</option>
                        <option <?php if ($itemsPerPage == 15) echo "selected" ?>>15</option>
                    </select>
                    <input type="submit" value="Go" class="btn btn-primary">
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="index.php" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="title">Filter by Title</label>
                            <input type="text" name="filterByTitle" value="" id="title" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for = "des">Filter by Description</label>
                            <input type="text" name="filterByDescription" value="" id="des" class="form-control">
                        </div>
                        <p class="col-md-12">
                            <button type="submit" class="btn btn-default mtop">Submit</button>
                        </p>
                    </div>
                </form>
            </div>
            <div class="col-sm-6">
                <form action="index.php" method="get">
                    <label for="search">Search</label>
                    <input type="text" name="search" value="" class="form-control" id="search">
                    <button type="submit" class="btn btn-default mtop">Search</button>
                </form>
            </div>
        </div>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th class="actions">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl = 0;
            foreach($bookList as $bookItem):
                $sl++; ?>
            <tr>
                <td><?= $sl + $pageStartFrom ?></td>
                <td><?= $bookItem['id'] ?></td>
                <td><?= $bookItem['book_title'] ?></td>
                <td><?= $bookItem['description'] ?></td>
                <td class="actions">
                    <a href="view.php?id=<?= $bookItem['id'] ?>" class="btn btn-primary">View</a>
                    <a href="edit.php?id=<?= $bookItem['id'] ?>" class="btn btn-primary">Edit</a>
                    <a href="delete.php?id=<?= $bookItem['id'] ?>" class="btn btn-danger dl">Delete</a>
                    <a href="trash.php?id=<?= $bookItem['id'] ?>" class="btn btn-warning">Trash</a>
                    <a href="mail.php?id=<?= $bookItem['id'] ?>&email=<?= $bookItem['email'] ?>" class="btn btn-primary">Email</a>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <!-- Pagination -->
        <?php
        $showPg = strtoupper($_SERVER['REQUEST_METHOD']=='POST') || isset($_GET['search']);
        if(!$showPg) : ?>

        <?php if($totalItem > $itemsPerPage) : ?>
        <nav>
            <ul class="pagination">

                <?php if($pageNumber > 1): ?>
                <li>
                    <a href="index.php?pageNumber=<?php echo $pageNumber-1 ?>" aria-label="Previous">
                        Prev
                    </a>
                </li>
                <?php endif; ?>

                <?php echo $pagination ?>

                <?php if($pageNumber < $totalPages): ?>
                <li>
                    <a href="index.php?pageNumber=<?php echo $pageNumber + 1 ?>" aria-label="Next">
                        Next
                    </a>
                </li>

                <?php endif; ?>

            </ul>
        </nav>

        <?php endif; ?>

        <div class="dlb">
            <p>
                <a href="pdf.php" class="btn btn-success">Download As PDF</a>
                <a href="xl.php" class="btn btn-success">Download As XLs</a>
                <a href="mail.php" class="btn btn-success">Mail To Friend</a>
            </p>
        </div>
        <?php endif; ?>
    </div>


</div>

<?php include_once '../../../includes/footer.php' ?>

<script>
    $('#message').delay(3000).fadeOut();
</script>

<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated?>
        ];
        $( "#title" ).autocomplete({
            source: availableTags
        });
    } );

    $( function() {
        var availableTags = [
            <?php echo $comma_separatedDes ?>
        ];
        $( "#des" ).autocomplete({
            source: availableTags
        });
    } );
</script>
</body>
</html>