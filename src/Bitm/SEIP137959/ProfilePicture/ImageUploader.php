<?php
namespace App\Bitm\SEIP137959\ProfilePicture;

use App\Bitm\SEIP137959\Message\Message;

class ImageUploader
{
    public $id = '',
        $name = '',
        $image_name = '',
        $conn;

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'atomicprojects');
    }

    public function prepare($data = '')
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if(array_key_exists('image_name', $data)) {
            $this->image_name = $data['image_name'];
        }

        return $this;
    }

    public function index()
    {
        $_all = array();
        $query = "SELECT * FROM `profiles` WHERE deleted_at IS NULL";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_all[] = $row;
        }
        return $_all;
    }

    public function indexTrash()
    {
        $_all = array();
        $query = "SELECT * FROM `profiles` WHERE deleted_at IS NOT NULL";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_all[] = $row;
        }
        return $_all;
    }

    public function paginator($start, $limit)
    {
        $_all = array();
        $query = "SELECT * FROM `profiles` WHERE deleted_at IS NULL LIMIT {$start},{$limit}";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_all[] = $row;
        }
        return $_all;
    }

    public function view()
    {
        $query = "SELECT * FROM `profiles` WHERE `id` = {$this->id}";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    public function store()
    {
        $query = "INSERT INTO `profiles` (`name`, `image_name`) VALUES ('".$this->name."', '".$this->image_name."')";
        if( mysqli_query($this->conn, $query)) {
            Message::message("Data successfully stored", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        if( !empty($this->image_name) ) {
            $query = "UPDATE `profiles` SET `name` = '" . $this->name . "', `image_name` = '" . $this->image_name . "' WHERE `id` = " . $this->id;
        } else {
            $query = "UPDATE `profiles` SET `name` = '" . $this->name . "' WHERE `id` = " . $this->id;
        }
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Updated", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function delete()
    {

        $query = "DELETE FROM `profiles` WHERE `id` = " . $this->id;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }

    }

    public function deleteMultiple($ids)
    {
        $query = "DELETE FROM `profiles` WHERE `id` IN({$ids})";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All data successfully deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function trash()
    {
        $time = time();
        $query = "UPDATE `profiles` SET `deleted_at` = '{$time}' WHERE `id` = " . $this->id;

        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Trashed", "warning");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function recover()
    {
        $query = "UPDATE `profiles` SET `deleted_at` = NULL WHERE `id` = " . $this->id;

        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Recovered", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function recoverMultiple($ids)
    {
        $query = "UPDATE `profiles` SET `deleted_at` = NULL WHERE `id` IN ({$ids})";

        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Recovered", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function count()
    {
        $query = "SELECT count(*) AS totalItems FROM `profiles` WHERE deleted_at IS NULL";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItems'];
    }

}