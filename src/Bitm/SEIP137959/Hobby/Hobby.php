<?php
namespace App\Bitm\SEIP137959\Hobby;

use App\Bitm\SEIP137959\Message\Message;

class Hobby
{
    public $id = '',
        $hobby = '',
        $conn,
        $deleted_at;

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'atomicprojects');
    }

    public function prepare($data = '')
    {
        if(array_key_exists('hobby', $data)) {
            $this->hobby = $data['hobby'];
        }
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        return $this;
    }

    public function index()
    {
        $_allRslts = array();
        $query = "SELECT * FROM `hobbies` WHERE deleted_at IS NULL";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }

    public function indexTrashed()
    {
        $_allRslts = array();
        $query = "SELECT * FROM `hobbies` WHERE deleted_at IS NOT NULL";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }

    public function view()
    {
        $query = "SELECT * FROM `hobbies` WHERE `id` =". $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }


    public function store()
    {
        $query = "INSERT INTO `hobbies` (`hobby`) VALUES ('".$this->hobby."')";
        if( mysqli_query($this->conn, $query)) {
            Message::message("Data successfully stored", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }


    public function update()
    {
        $query = "UPDATE `hobbies` SET `hobby` = '". $this->hobby ."' WHERE `id` = ". $this->id;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Updated", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }


    public function delete()
    {

        $query = "DELETE FROM `hobbies` WHERE `id` = " . $this->id . "";
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }

    }

    public function deleteMultiple($ids)
    {
        $query = "DELETE FROM `hobbies` WHERE `id` IN({$ids})";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All data successfully deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }


    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `hobbies` SET `deleted_at` = '". $this->deleted_at ."' WHERE `id` = ".$this->id ;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully trashed", "warning");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function recover()
    {
        $query = "UPDATE `hobbies` SET `deleted_at` = NULL WHERE `id` = ".$this->id ;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully recovered", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function recoverMultiple($ids)
    {
        $query = "UPDATE `hobbies` SET `deleted_at` = NULL WHERE `id` IN ({$ids})";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All data successfully recovered", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function count()
    {
        $query = "SELECT count(*) AS totalItems FROM `hobbies` WHERE deleted_at IS NULL";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItems'];

    }

    public function paginator($start, $limit)
    {
        $_allRslts = [];
        $query = "SELECT * FROM `hobbies` WHERE `deleted_at` IS NULL LIMIT {$start},{$limit}";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }
}