<?php
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Hobby\Hobby;

$hobby = new Hobby();

$hobbyAll = implode(", ", $_POST['mark']);

$_POST['hobby'] = $hobbyAll;

$hobby->prepare($_POST);
$hobby->store();
