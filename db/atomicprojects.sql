-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2016 at 08:58 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicprojects`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdays`
--

CREATE TABLE `birthdays` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `date` date NOT NULL,
  `deleted_at` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthdays`
--

INSERT INTO `birthdays` (`id`, `name`, `date`, `deleted_at`) VALUES
(17, 'Rawan', '1986-07-30', '1467227046'),
(18, 'Emran', '2016-06-09', NULL),
(19, 'Rawan raisa', '2007-12-27', '1467221553'),
(20, 'Sarwar', '2012-07-18', NULL),
(21, 'Emran', '1993-06-23', NULL),
(22, 'Birthday one', '2007-07-16', NULL),
(23, 'Birthday two', '2001-02-05', NULL),
(24, 'Srijan', '2016-06-09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `book_title` varchar(50) NOT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `description` text,
  `description_html` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `book_title`, `deleted_at`, `email`, `description`, `description_html`) VALUES
(21, 'Alice in wonderland', '1469037139', 'afadf@sdf.com', NULL, NULL),
(22, 'Book one', NULL, 'emran04ch@gmail.com', NULL, NULL),
(23, 'Book two', NULL, 'emran04ch@gmail.com', '<p>This is updated description for this book.</p>', 'This is updated description for this book.'),
(24, 'Book three', NULL, 'emran04ch@gmail.com', '<p>Description for book three.</p>', 'Description for book three.'),
(25, 'Book four', NULL, 'emran04ch@gmail.com', '<p>Descriptio for book four.</p>', 'Descriptio for book four.'),
(26, 'Book five', NULL, '', NULL, NULL),
(27, 'Book six', NULL, '', NULL, NULL),
(28, 'My Book', NULL, 'emran04ch@gmail.com', NULL, NULL),
(29, 'New Book', NULL, 'emran04ch@gmail.com', '<p>This is new book</p>', 'This is new book');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `deleted_at` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city`, `deleted_at`) VALUES
(32, 'Chittagong, Rangpur', '1467223458'),
(33, 'Chittagong', NULL),
(36, 'Chittagong, Dhaka', NULL),
(37, 'Chittagong, Rangpur', NULL),
(38, 'Dhaka, Rangpur', NULL),
(39, 'Chittagong, Dhaka', NULL),
(40, 'Rangpur', NULL),
(41, 'Dhaka', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` int(11) NOT NULL,
  `gender` varchar(55) NOT NULL,
  `deleted_at` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `gender`, `deleted_at`) VALUES
(10, 'Male', NULL),
(11, 'Female', NULL),
(12, 'Male', NULL),
(14, 'Others', NULL),
(15, 'Male', 1467223126),
(16, 'Male', NULL),
(17, 'Female', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `hobby` text NOT NULL,
  `deleted_at` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobby`, `deleted_at`) VALUES
(20, 'Football, Hiking, Travelling', NULL),
(21, 'Hiking, Travelling, Fishing', NULL),
(22, 'Football, Hiking, Travelling', NULL),
(23, 'Football, Hiking, Fishing', NULL),
(25, 'Hiking', '1467222836'),
(26, 'Travelling', NULL),
(27, 'Football, Hiking', NULL),
(28, 'Hiking, Travelling', NULL),
(29, 'Football, Hiking', NULL),
(30, 'Football, Hiking', NULL),
(31, 'Travelling, Fishing', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `image_name`, `deleted_at`) VALUES
(8, 'gergeg', '14669583829321399323_cf93173702_c.jpg', NULL),
(11, 'Osean', '1467206941Photo1671.jpg', NULL),
(12, 'Sea', '1467206961Photo1652.jpg', NULL),
(13, 'Other picture', '1467207043Photo1663.jpg', NULL),
(14, 'Nature', '1467224167Photo1658.jpg', NULL),
(15, 'Nature', '1467224200Photo1662.jpg', NULL),
(16, 'Nature', '1467224258Photo1589.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `name`, `email`, `deleted_at`) VALUES
(6, 'Emran', 'emran@emrangist.com', NULL),
(10, '', 'fafa@sdf.fds', NULL),
(11, 'Mahi', 'afaf@afafl.com', NULL),
(15, 'Romel', 'afaf@afafl.com', NULL),
(16, '', 'efghij@gmail.com', NULL),
(17, '', 'emran04ch@gmail.com', NULL),
(18, '', 'emran04ch@gmail.com', NULL),
(19, 'Emran Hossen', 'emran04ch@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summarys`
--

CREATE TABLE `summarys` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summarys`
--

INSERT INTO `summarys` (`id`, `name`, `summary`, `deleted_at`) VALUES
(1, 'McDonalds', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias eligendi ex mollitia placeat quia, velit.\r\n\r\nupdate', NULL),
(2, 'Facebook', 'another descriptions update', NULL),
(4, '', 'Blanditiis delectus esse quibusdam soluta voluptas. Accusantium dolore non quia similique tempore!', NULL),
(5, '', 'Accusantium dolore non quia similique tempore!. Blanditiis delectus esse quibusdam soluta voluptas.', NULL),
(9, '', 'Blanditiis delectus esse quibusdam soluta voluptas. Accusantium dolore non quia similique tempore!	View update', NULL),
(10, 'Apple', 'Blanditiis delectus esse quibusdam soluta voluptas. Accusantium dolore non quia similique tempore!	View', NULL),
(11, '', 'Blanditiis delectus esse quibusdam soluta voluptas. Accusantium dolore non quia similique tempore!	View', NULL),
(12, 'Google', 'Summary for google.', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdays`
--
ALTER TABLE `birthdays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summarys`
--
ALTER TABLE `summarys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdays`
--
ALTER TABLE `birthdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `summarys`
--
ALTER TABLE `summarys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
