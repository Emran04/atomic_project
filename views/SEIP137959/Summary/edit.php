<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Summary\Summary;


$summary = new Summary();

$summarySingle = $summary->prepare($_GET)->view();

?>
<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Edit Summary</h1>
        </div>

        <form action="update.php" method="post">
            <input type="hidden" name="id" value="<?= $summarySingle['id'] ?>">
            <label for="name">Name: </label>
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control" value="<?= $summarySingle['name'] ?>">
            </div>
            <label for="summary">Summary: </label>
            <div class="form-group">
                <textarea name="summary" id="summary" rows="8" class="form-control"><?= $summarySingle['summary'] ?></textarea>
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>

    </div>
</div>

</body>
</html>