<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Projects</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/style.css">
</head>
<body>


<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>All Projects</h1>
        </div>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>SL</th>
                <th>Project</th>
                <th class="actions">View</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Book</td>
                    <td class="actions">
                        <a href="views/SEIP137959/Book/index.php" class="btn btn-primary">View</a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Email</td>
                    <td class="actions">
                        <a href="views/SEIP137959/Email/index.php" class="btn btn-primary">View</a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Hobby</td>
                    <td class="actions">
                        <a href="views/SEIP137959/Hobby/index.php" class="btn btn-primary">View</a>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Profile Picture</td>
                    <td class="actions">
                        <a href="views/SEIP137959/ProfilePicture/index.php" class="btn btn-primary">View</a>
                    </td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Gender</td>
                    <td class="actions">
                        <a href="views/SEIP137959/Gender/index.php" class="btn btn-primary">View</a>
                    </td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Organization summary</td>
                    <td class="actions">
                        <a href="views/SEIP137959/Summary/index.php" class="btn btn-primary">View</a>
                    </td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>City</td>
                    <td class="actions">
                        <a href="views/SEIP137959/City/index.php" class="btn btn-primary">View</a>
                    </td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Birthday</td>
                    <td class="actions">
                        <a href="views/SEIP137959/Birthday/index.php" class="btn btn-primary">View</a>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
<script src="../../../resources/jquery/jquery.min.js"></script>

</body>
</html>