<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\City\City;
use App\Bitm\SEIP137959\Message\Message;

$city = new City();

$cities = $city->indexTrashed();

if(!empty($_SESSION['message'])) {
    $message = Message::message();
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cities</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/css/style.css">
    <link rel="stylesheet" href="../../../resources/sweetalert/dist/sweetalert.css">
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">
                HOME
            </a>
        </div>
    </div>
</nav>

<!-- Main Container -->
<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>All Trashed Cities</h1>
        </div>

        <!-- Alert Message -->
        <?php if(isset($message)) : ?>
            <div id="message" class="alert alert-<?php echo $message['lvl'];  ?>" role="alert">
                <?php echo $message['message'];  ?>
            </div>
        <?php endif; ?>

        <form action="deletemultiple.php" method="post" id="tableAll">

            <p>
                <a href="create.php" class="btn btn-primary">Create New City</a>
                <button type="submit" class="btn btn-success" id="recoverAll">Recover All</button>
                <button type="submit" class="btn btn-danger" id="delAll">Delete All</button>
            </p>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        <label for="sAll">
                            <input type="checkbox" id="sAll"> Select All
                        </label>
                    </th>
                    <th>SL</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="actions">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sl = 0;
                foreach($cities as $city):
                    $sl++; ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="mark[]" value="<?= $city['id'] ?>">
                        </td>
                        <td><?= $sl ?></td>
                        <td><?= $city['id'] ?></td>
                        <td><?= $city['city'] ?></td>
                        <td class="actions">
                            <a href="view.php?id=<?= $city['id'] ?>" class="btn btn-primary">View</a>
                            <a href="recover.php?id=<?= $city['id'] ?>" class="btn btn-success">Recover</a>
                            <a href="delete.php?id=<?= $city['id'] ?>" class="btn btn-danger dl">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </form>


    </div>


</div>

<?php include_once '../../../includes/footer.php' ?>

<script>
    $('#recoverAll').on("click", function (evt) {
        $('#tableAll').attr('action', 'recovermultiple.php').submit();
    });

    $('#delAll').on("click", function (evt) {
        evt.preventDefault();
        swal({
            title: "Are you sure?",
            text: "It will permanently delete All records!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete all!",
            cancelButtonText: "No, cancel !",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                $('#tableAll').attr('action', 'deletemultiple.php').submit();
            } else {
                return false;
            }
        });
    });

    $('#sAll').click(function(event) {
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });

</script>
</body>
</html>