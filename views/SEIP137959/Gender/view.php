<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Gender\Gender;


$gender = new Gender();


$genderSingle = $gender->prepare($_GET)->view();

?>
<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Gender</h1>
        </div>

        <div class="list-group">
            <div class="list-group-item">ID: <?= $genderSingle['id'] ?></div>
            <div class="list-group-item">Gender: <?= $genderSingle['gender'] ?></div>
        </div>

        <p>
            <a href="edit.php?id=<?= $genderSingle['id'] ?>" class="btn btn-primary">Edit</a>
            <a href="delete.php?id=<?= $genderSingle['id'] ?>" class="btn btn-danger">Delete</a>
        </p>

    </div>
</div>

</body>
</html>