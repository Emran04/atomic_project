<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Create Summary</h1>
        </div>

        <form action="store.php" method="post">
            <label for="name">Name: </label>
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control" placeholder="Name">
            </div>
            <label for="summary">Summary: </label>
            <div class="form-group">
                <textarea name="summary" id="summary" rows="8" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

    </div>
</div>

</body>
</html>