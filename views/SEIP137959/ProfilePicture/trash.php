<?php
require_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;

$profile = new ImageUploader();

$profiles = $profile->prepare($_GET)->trash();
