<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Birthday\Birthday;


$birthday = new Birthday();

$birthday->prepare($_POST)->update();
