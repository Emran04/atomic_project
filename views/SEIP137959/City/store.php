<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\City\City;

$cities = implode(', ', $_POST['city']);

$_POST['city'] = $cities;

$birthday = new City();

$birthday->prepare($_POST)->store();

