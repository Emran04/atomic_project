<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Gender\Gender;


$gender = new Gender();


$genderSingle = $gender->prepare($_GET)->view();

?>
<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Edit Gender</h1>
        </div>

        <form action="update.php" method="post">
            <input type="hidden" name="id" value="<?= $genderSingle['id']  ?>">
            <div class="form-group">
                <h3>Gender: </h3>
                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="Male" <?php if($genderSingle['gender'] == 'Male') echo "checked" ?>>
                        Male
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="Female" <?php if($genderSingle['gender'] == 'Female') echo "checked" ?>>
                        Female
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="gender" value="Others" <?php if($genderSingle['gender'] == 'Others') echo "checked" ?>>
                        Others
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>

    </div>
</div>

</body>
</html>