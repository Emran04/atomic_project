<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;

$profile = new ImageUploader();

$profiles = $profile->prepare($_GET)->view();

?>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <div class="page-header">
            <h1>Profile</h1>
        </div>

        <div class="profile">
            <div class="row">
                <div class="col-sm-4">
                    <div class="list-group">
                        <div class="list-group-item">ID: <?= $profiles['id'] ?></div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <img src="../../../resources/images/<?= $profiles['image_name'] ?>" alt="Image" height="320px" class="img-rounded">
                </div>
            </div>
            <p>
                <a href="edit.php?id=<?= $profiles['id'] ?>" class="btn btn-primary">Edit</a>
                <a href="delete.php?id=<?= $profiles['id'] ?>" class="btn btn-danger">Delete</a>
            </p>
        </div>

    </div>
</div>
</body>
</html>