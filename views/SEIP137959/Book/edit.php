<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Book\Book;

$book = new Book();

$singleBook = $book->prepare($_GET)->view();

?>


<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Edit Book</h1>
        </div>

        <p>ID: <?= $singleBook['id'] ?></p>
        <form action="update.php" method="post">
            <div class="form-group">
                <input type="hidden" name="id" value="<?= $singleBook['id'] ?>">
                <label for="book_title">Book Title</label>
                <input type="text" name="book_title" class="form-control" id="book_title" placeholder="Book Title" value="<?= $singleBook['book_title'] ?>">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" value="<?= $singleBook['email'] ?>">
            </div>
            <div class="form-group">
                <label>Description:</label>
                <textarea class="form-control" rows="5" id="mytextarea" name="description"><?= $singleBook['description'] ?></textarea>
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>

    </div>
</div>

<?php include_once '../../../includes/footer.php' ?>
<script>
    tinymce.init({
        selector: '#mytextarea'
    });
</script>
</body>
</html>