<?php
namespace App\Bitm\SEIP137959\Email;

use App\Bitm\SEIP137959\Message\Message;

class Email
{
    public $id = '';
    public $email = '';
    public $name = '';
    public $filterByName = '';
    public $filterByEmail = '';
    public $search="";
    public $deleted_at = '';
    public $conn;

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'atomicprojects');
    }

    public function prepare($data = '')
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        if(array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

        if(array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }

        if (array_key_exists("filterByName", $data)) {
            $this->filterByName = $data['filterByName'];
        }

        if (array_key_exists("filterByEmail", $data)) {
            $this->filterByEmail = $data['filterByEmail'];
        }

        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }

        return $this;
    }

    public function index()
    {
        $whereClause= "1=1";

        if(!empty($this->filterByName)){
            $whereClause.=" AND  name LIKE '%".$this->filterByName."%'";
        }

        if(!empty($this->filterByEmail)){
            $whereClause.=" AND  email LIKE '%".$this->filterByEmail."%'";
        }

        if(!empty($this->search)){
            $whereClause.=" AND  name LIKE '%".$this->search."%' OR email LIKE '%".$this->search."%'";
        }

        $_allEmails = array();

        $query = "SELECT * FROM `subscriptions` WHERE `deleted_at` IS NULL AND {$whereClause}";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allEmails[] = $row;
        }

        return $_allEmails;
    }


    public function indexTrashed()
    {
        $_allRslts = array();
        $query = "SELECT * FROM `subscriptions` WHERE deleted_at IS NOT NULL";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }

    public function view()
    {
        $query = "SELECT * FROM `subscriptions` WHERE `id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    public function store()
    {
        $query = "INSERT INTO `subscriptions` (`name`,`email`) VALUES ('".$this->name."', '".$this->email."')";
        if( mysqli_query($this->conn, $query)) {
            Message::message("Data successfully stored", 'success');
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        $query = "UPDATE `subscriptions` SET `name` = '". $this->name ."', `email` = '". $this->email ."' WHERE `id` = ". $this->id;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Updated", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `subscriptions` WHERE `id` = ". $this->id;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Deleted", 'danger');
            header('Location: index.php');
        } else {
            echo "Error";
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `subscriptions` SET `deleted_at` = '". $this->deleted_at ."' WHERE `id` = ".$this->id ;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully trashed", "warning");
            header('Location: index.php');
        } else {
            echo "Error";
        }

    }

    public function recover()
    {
        $query = "UPDATE `subscriptions` SET `deleted_at` = NULL WHERE `id` = ".$this->id ;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully recovered", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function recoverMultiple($ids)
    {
        $query = "UPDATE `subscriptions` SET `deleted_at` = NULL WHERE `id` IN ({$ids})";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All data successfully recovered", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function deleteMultiple($ids)
    {
        $query = "DELETE FROM `subscriptions` WHERE `id` IN({$ids})";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All data successfully deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function count()
    {
        $query = "SELECT count(*) AS totalItems FROM `subscriptions` WHERE deleted_at IS NULL";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItems'];

    }

    public function paginator($start, $limit)
    {
        $_allRslts = [];
        $query = "SELECT * FROM `subscriptions` WHERE `deleted_at` IS NULL LIMIT {$start},{$limit}";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }

    public function getAllName()
    {
        $_all = array();
        $query = "SELECT `name` FROM `subscriptions` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_all[] = $row['name'];
        }

        return $_all;
    }

    public function getAllEmail()
    {
        $_all = array();
        $query = "SELECT `email` FROM `subscriptions` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_all[] = $row['email'];
        }

        return $_all;
    }
}