<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\City\City;

$city = new City();

$city = $city->prepare($_GET)->view();
?>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>City</h1>
        </div>

        <div class="list-group">
            <div class="list-group-item">ID: <?= $city['id'] ?></div>
            <div class="list-group-item">City: <?= $city['city'] ?></div>
        </div>
        <p>
            <a href="edit.php?id=<?= $city['id'] ?>" class="btn btn-primary">Edit</a>
            <a href="delete.php?id=<?= $city['id'] ?>" class="btn btn-danger">Delete</a>
        </p>

    </div>
</div>

</body>
</html>