<script src="../../../resources/jquery/jquery.min.js"></script>
<!--<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>-->
<script src="../../../resources/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>

<script>

    $('.dl').click(function(evt){

        evt.preventDefault();
        var linkURL = $(this).attr("href");
        swal({
            title: "Are you sure?",
            text: "It will permanently delete this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel !",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                window.location.href = linkURL;
            } else {
                return false;
            }
        });
    });


</script>