<?php
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Hobby\Hobby;

$hobby = new Hobby();

$hobby->prepare($_GET);

$rslt = $hobby->view();

$rslts = explode(", ", $rslt['hobby']);


?>


<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h2>Update hobbies</h2>
        </div>

        <form role="form" action="update.php" method="post">
            <input type="hidden" name="id" value="<?php echo $hobby->id; ?>">
            <div class="input-group">
                <label><input type="checkbox" name="mark[]" value="Football" <?php if(in_array('Football', $rslts)) { echo "checked"; } ?>> Playing Football</label>
            </div>
            <div class="input-group">
                <label><input type="checkbox" name="mark[]" value="Hiking" <?php if(in_array('Hiking', $rslts)) { echo "checked"; } ?>> Hiking</label>
            </div>
            <div class="input-group">
                <label><input type="checkbox" name="mark[]" value="Travelling" <?php if(in_array('Travelling', $rslts)) { echo "checked"; } ?>> Travelling</label>
            </div>
            <div class="input-group">
                <label><input type="checkbox" name="mark[]" value="Fishing" <?php if(in_array('Fishing', $rslts)) { echo "checked"; } ?>> Fishing</label>
            </div>
            <div class="input-group">
                <input type="submit" class="btn btn-success" value="Update">
            </div>
        </form>

    </div>
</div>

</body>
</html>

