<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;

$profile = new ImageUploader();

$profiles = $profile->prepare($_POST)->view();

if(isset($_FILES['image']) && !empty(['image']['name'])) {

    $path = $_SERVER['DOCUMENT_ROOT'] . "/AtomicProjects/resources/images/" . $profiles['image_name'];

    unlink($path);

    $image_name = time() . $_FILES['image']['name'];
    $image_temp_location = $_FILES['image']['tmp_name'];

    move_uploaded_file($image_temp_location, "../../../resources/images/" . $image_name);

    $_POST['image_name'] = $image_name;

}
$profile->prepare($_POST)->update();
