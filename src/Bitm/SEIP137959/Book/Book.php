<?php
namespace App\Bitm\SEIP137959\Book;

use App\Bitm\SEIP137959\Message\Message;

class Book
{
    public $id = '',
        $title = '',
        $email = '',
        $conn,
        $deleted_at;
    public $description="";
    public $filterByTitle="";
    public $filterByDescription="";
    public $search="";

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'atomicprojects');
    }

    public function prepare($data = '')
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('book_title', $data)) {
            $this->title = $data['book_title'];
        }
        if(array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }

        if (array_key_exists("description", $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists("filterByTitle", $data)) {
            $this->filterByTitle = $data['filterByTitle'];
        }
        if (array_key_exists("filterByDescription", $data)) {
            $this->filterByDescription = $data['filterByDescription'];
        }
        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }

        return $this;
    }

    public function index()
    {
        $whereClause= "1=1";

        if(!empty($this->filterByTitle)){
            $whereClause.=" AND  book_title LIKE '%".$this->filterByTitle."%'";
        }

        if(!empty($this->filterByDescription)){
            $whereClause.=" AND  description LIKE '%".$this->filterByDescription."%'";
        }

        if(!empty($this->search)){
            $whereClause.=" AND  description LIKE '%".$this->search."%' OR book_title LIKE '%".$this->search."%'";
        }

        $_allBooks = array();
        $query = "SELECT * FROM `books` WHERE `deleted_at` IS NULL AND ".$whereClause;
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allBooks[] = $row;
        }

        return $_allBooks;
    }

    public function indexTrashed()
    {
        $_allRslts = array();
        $query = "SELECT * FROM `books` WHERE deleted_at IS NOT NULL";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }

    public function view()
    {
        $query = "SELECT * FROM `books` WHERE `id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    public function store()
    {
        $des = strip_tags($this->description);

        $query = "INSERT INTO `books` (`book_title`, `email`, `description_html`, `description`) VALUES ('".$this->title."', '{$this->email}', '{$des}', '{$this->description}')";
        if( mysqli_query($this->conn, $query)) {
            Message::message("Data successfully stored", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        $des = strip_tags($this->description);
        $query = "UPDATE `books` SET `book_title` = '". $this->title ."', `email` = '{$this->email}', `description_html` = '{$des}', `description` = '{$this->description}' WHERE `books`.`id` = ". $this->id;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Updated", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `books` WHERE `id` = " .$this->id;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `books` SET `deleted_at` = '". $this->deleted_at ."' WHERE `id` = ".$this->id ;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully trashed", "warning");
            header('Location: index.php');
        } else {
            echo "Error";
        }

    }

    public function recover()
    {
        $query = "UPDATE `books` SET `deleted_at` = NULL WHERE `id` = ".$this->id ;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully trashed", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function recoverMultiple($ids = array())
    {
        $idr = implode(',', $ids);
        $query = "UPDATE `books` SET `deleted_at` = NULL WHERE `books`.`id` IN(".$idr.")";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All Data successfully trashed", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function deleteMultiple($ids = array())
    {
        $idr = implode(',', $ids);
        $query = "DELETE FROM `books` WHERE `books`.`id` IN(".$idr.")";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All Data successfully deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function count()
    {
        $query = "SELECT count(*) AS totalItems FROM `books` WHERE deleted_at IS NULL";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItems'];

    }

    public function paginator($start, $limit)
    {
        $_allBooks = array();
        $query = "SELECT * FROM `books` WHERE deleted_at IS NULL LIMIT {$start}, {$limit}";

        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allBooks[] = $row;
        }

        return $_allBooks;

    }

    public function getAllTitle()
    {
        $_allBook = array();
        $query = "SELECT `book_title` FROM `books` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row['book_title'];
        }

        return $_allBook;
    }

    public function getAllDescription()
    {
        $_allBook = array();
        $query = "SELECT `description_html` FROM `books` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allBook[] = $row['description_html'];
        }

        return $_allBook;
    }

}