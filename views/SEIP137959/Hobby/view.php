<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Hobby\Hobby;

$hobby = new Hobby();

$hobby->prepare($_GET);

$rslt = $hobby->view();

$rslts = explode(", ", $rslt['hobby']);


?>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h2>Hobbies</h2>
        </div>

        <ul class="list-group">
            <li class="list-group-item">ID: <?= $rslt['id'] ?></li>
            <li class="list-group-item">Hobbies: <?= $rslt['hobby'] ?></li>
        </ul>

        <p>
            <a href="edit.php?id=<?= $rslt['id'] ?>" class="btn btn-primary">Edit</a>
            <a href="delete.php?id=<?= $rslt['id'] ?>" class="btn btn-danger">Delete</a>
        </p>

    </div>
</div>

</body>
</html>

