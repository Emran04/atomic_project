<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Email\Email;
use App\Bitm\SEIP137959\Message\Message;

$email = new Email();


if(!empty($_SESSION['message'])) {
    $message = Message::message();
}

if( array_key_exists("itemPerPage", $_SESSION) ) {
    if( array_key_exists("itemPerPage", $_GET) ) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
} else {
    $_SESSION['itemPerPage'] = 5;
}

$itemsPerPage = $_SESSION['itemPerPage'];

$totalItem = $email->count();

$totalPages = ceil($totalItem / $itemsPerPage);

if(isset($_GET) && array_key_exists("page", $_GET)) {
    $pageNumber = $_GET['page'];
}else {
    $pageNumber = 1;
}

$pagination = "";

for($i = 1; $i <= $totalPages; $i++) {
    $class = ($i == $pageNumber) ? "active" : "";
    $pagination .= "<li class='{$class}'><a href='index.php?page={$i}'>$i</a></li>";
}

$pageStartFrom = $itemsPerPage * ($pageNumber - 1);




if(strtoupper($_SERVER['REQUEST_METHOD']=='GET')) {
    $emailList = $email->paginator($pageStartFrom, $itemsPerPage);
}

if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')) {
    $emailList = $email->prepare($_POST)->index();
}

if((strtoupper($_SERVER['REQUEST_METHOD']=='GET'))&& isset($_GET['search'])) {
    $emailList = $email->prepare($_GET)->index();
}

$availableName = $email->getAllName();

$comma_separated = '"'.implode('","', $availableName).'"';

$availableEmail = $email->getAllEmail();

$comma_separatedEmail = '"'.implode('","', $availableEmail).'"';

?>

<?php include_once '../../../includes/header.php' ?>

<div class="container-fluid">

    <?php include_once '../../../includes/sidebar.php' ?>

    <div class="col-md-8 col-md-offset-1">

        <div class="page-header">
            <h1>Emails</h1>
        </div>
        <?php if(isset($message)) : ?>
            <div id="message" class="alert alert-<?php echo $message['lvl'];  ?>" role="alert">
                <?php echo $message['message'];  ?>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-6">
                <p>
                    <a href="create.php" class="btn btn-primary">Create New Email</a>
                </p>
            </div>
            <div class="col-md-6 itemsPPage">
                <form action="" method="get">
                    <label for="num">Items Per Page:</label>
                    <select class="form-control items" name="itemPerPage" id="num">
                        <option <?php if($itemsPerPage == 5) echo "selected" ?>>5</option>
                        <option <?php if($itemsPerPage == 10) echo "selected" ?>>10</option>
                        <option <?php if ($itemsPerPage == 15) echo "selected" ?>>15</option>
                    </select>
                    <input type="submit" value="Go" class="btn btn-primary">
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <form action="index.php" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="title">Filter by Name</label>
                            <input type="text" name="filterByName" value="" id="title" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="des">Filter by Email</label>
                            <input type="text" name="filterByEmail" value="" id="des" class="form-control">
                        </div>
                        <p class="col-md-12">
                            <button type="submit" class="btn btn-default mtop">Submit</button>
                        </p>
                    </div>
                </form>
            </div>
            <div class="col-sm-6">
                <form action="index.php" method="get">
                    <label for="search">Search</label>
                    <input type="text" name="search" id="search" class="form-control">
                    <button type="submit" class="btn btn-default mtop">Search</button>
                </form>
            </div>
        </div>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th class="actions">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl = 0;
            foreach($emailList as $emailItem):
                $sl++; ?>
            <tr>
                <td><?= $sl + $pageStartFrom ?></td>
                <td><?= $emailItem['id'] ?></td>
                <td><?= $emailItem['name'] ?></td>
                <td><?= $emailItem['email'] ?></td>
                <td class="actions">
                    <a href="view.php?id=<?= $emailItem['id'] ?>" class="btn btn-primary">View</a>
                    <a href="edit.php?id=<?= $emailItem['id'] ?>" class="btn btn-primary">Edit</a>
                    <a href="delete.php?id=<?= $emailItem['id'] ?>" class="btn btn-danger dl">Delete</a>
                    <a href="trash.php?id=<?= $emailItem['id'] ?>" class="btn btn-warning">Trash</a>
                    <a href="mail.php?id=<?= $emailItem['id'] ?>&email=<?= $emailItem['email'] ?>" class="btn btn-primary">Email</a>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php
        $showPg = strtoupper($_SERVER['REQUEST_METHOD']=='POST') || isset($_GET['search']);
        if(!$showPg) : ?>

        <!-- Pagination -->

        <?php if($totalItem > $itemsPerPage) : ?>

            <nav>
                <ul class="pagination">
                    <?php if($pageNumber > 1) : ?>
                        <li>
                            <a href="index.php?page=<?= ($pageNumber-1) ?>" aria-label="Previous">
                                <span aria-hidden="true">Prev</span>
                            </a>
                        </li>
                    <?php endif ?>
                    <?= $pagination ?>
                    <?php if($pageNumber < $totalPages): ?>
                        <li>
                            <a href="index.php?page=<?= ($pageNumber+ 1) ?>" aria-label="Next">
                                <span aria-hidden="true">Next</span>
                            </a>
                        </li>
                    <?php endif ?>
                </ul>
            </nav>

        <?php endif ?>

        <!-- End Pagination -->

            <div class="dlb">
                <p>
                    <a href="pdf.php" class="btn btn-success">Download As PDF</a>
                    <a href="xl.php" class="btn btn-success">Download As XLs</a>
                    <a href="mail.php" class="btn btn-success">Mail To Friend</a>
                </p>
            </div>

        <?php endif; ?>

    </div>
</div>

<?php include_once '../../../includes/footer.php' ?>

<script>
    $('#message').delay(3000).fadeOut();
</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated ?>
        ];
        $( "#title" ).autocomplete({
            source: availableTags
        });
    } );

    $( function() {
        var availableTags = [
            <?php echo $comma_separatedEmail ?>
        ];
        $( "#des" ).autocomplete({
            source: availableTags
        });
    } );
</script>
</body>
</html>