<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Email\Email;

$email = new Email();

$email->prepare($_GET)->recover();