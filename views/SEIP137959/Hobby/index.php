<?php
session_start();
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\Hobby\Hobby;
use App\Bitm\SEIP137959\Message\Message;

$hobby = new Hobby();

$hobbies = $hobby->index();

if(!empty($_SESSION['message'])) {
    $message = Message::message();
}

if(array_key_exists("itemPerPage", $_SESSION)) {
    if(array_key_exists("itemPerPage", $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
} else {
    $_SESSION['itemPerPage'] = 5;
}

$itemPerPage = $_SESSION['itemPerPage'];

$totalItem = $hobby->count();

$totalPage = ceil($totalItem / $itemPerPage);

//var_dump($totalPage);

if(isset($_GET) && array_key_exists("page", $_GET)) {
    $pageNumber = $_GET['page'];
} else {
    $pageNumber = 1;
}

$pageStartFrom = $itemPerPage * ($pageNumber - 1);

$pagination = "";

for($i = 1; $i <= $totalPage; $i++) {
    $class = ($i == $pageNumber) ? "active" : "";
    $pagination .= "<li class='{$class}'><a href='index.php?page={$i}'>$i</a></li>";
}

$hobbies = $hobby->paginator($pageStartFrom, $itemPerPage);

?>

<?php include_once '../../../includes/header.php' ?>

<div class="container-fluid">

    <?php include_once '../../../includes/sidebar.php' ?>

    <div class="col-md-8 col-md-offset-1">

        <div class="page-header">
            <h1>Hobbies</h1>
        </div>
        
        <?php if(isset($message)) : ?>
            <div id="message" class="alert alert-<?php echo $message['lvl'];  ?>" role="alert">
                <?php echo $message['message'];  ?>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-6">
                <p>
                    <a href="create.php" class="btn btn-primary">Create New Hobby</a>
                </p>
            </div>
            <div class="col-md-6 itemsPPage">
                <form action="" method="get">
                    <label for="num">Items Per Page:</label>
                    <select class="form-control items" name="itemPerPage" id="num">
                        <option <?php if($itemPerPage == 5) echo "selected" ?>>5</option>
                        <option <?php if($itemPerPage == 10) echo "selected" ?>>10</option>
                        <option <?php if ($itemPerPage == 15) echo "selected" ?>>15</option>
                    </select>
                    <input type="submit" value="Go" class="btn btn-primary">
                </form>
            </div>
        </div>
        
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                <th class="actions">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl = 0;
            foreach($hobbies as $hobby):
                $sl++; ?>
                <tr>
                    <td><?= $sl ?></td>
                    <td><?= $hobby['id'] ?></td>
                    <td><?= $hobby['hobby'] ?></td>
                    <td class="actions">
                        <a href="view.php?id=<?= $hobby['id'] ?>" class="btn btn-primary">View</a>
                        <a href="edit.php?id=<?= $hobby['id'] ?>" class="btn btn-primary">Edit</a>
                        <a href="delete.php?id=<?= $hobby['id'] ?>" class="btn btn-danger dl">Delete</a>
                        <a href="trash.php?id=<?= $hobby['id'] ?>" class="btn btn-warning">Trash</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php if($totalItem > $itemPerPage) : ?>

            <nav>
                <ul class="pagination">
                    <?php if($pageNumber > 1) : ?>
                        <li>
                            <a href="index.php?page=<?= ($pageNumber-1) ?>" aria-label="Previous">
                                <span aria-hidden="true">Prev</span>
                            </a>
                        </li>
                    <?php endif ?>
                    <?= $pagination ?>
                    <?php if($pageNumber < $totalPage): ?>
                        <li>
                            <a href="index.php?page=<?= ($pageNumber+ 1) ?>" aria-label="Next">
                                <span aria-hidden="true">Next</span>
                            </a>
                        </li>
                    <?php endif ?>
                </ul>
            </nav>

        <?php endif ?>

    </div>
</div>

<?php include_once '../../../includes/footer.php' ?>

<script>
    $('#message').delay(3000).fadeOut();
</script>
</body>
</html>