<?php
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Book\Book;

$book = new Book();

$book->prepare($_GET);

$book->recover();