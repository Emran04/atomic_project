<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Birthday\Birthday;

$birthday = new Birthday();

$birthday = $birthday->prepare($_GET)->view();

$birthdate = $birthday['date'];

$time = strtotime($birthdate);
$bdate = date("d/m/y", $time);

?>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <div class="page-header">
            <h1>Birthday</h1>
        </div>

        <div class="list-group">
            <div class="list-group-item">ID: <?= $birthday['id'] ?></div>
            <div class="list-group-item">Name: <?= $birthday['name'] ?></div>
            <div class="list-group-item">Date: <?= $bdate ?></div>
        </div>

        <p>
            <a href="edit.php?id=<?= $birthday['id'] ?>" class="btn btn-primary">Edit</a>
            <a href="delete.php?id=<?= $birthday['id'] ?>" class="btn btn-danger dl">Delete</a>
        </p>


    </div>
</div>

</body>
</html>