<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Birthday\Birthday;

$birthday = new Birthday();

$birthday = $birthday->prepare($_GET)->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Birthday</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/css/style.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">
                HOME
            </a>
        </div>
    </div>
</nav>
<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <div class="page-header">
            <h1>Birthday</h1>
        </div>

        <form action="update.php" method="post">
            <input type="hidden" name="id" value="<?= $birthday['id'] ?>">
            <div class="form-group">
                <input type="text" name="name" placeholder="Name" class="form-control" value="<?= $birthday['name'] ?>">
            </div>
            <div class="form-group">
                <label for="date"></label>
                <input type="date" name="date" id="date" class="form-control" value="<?= $birthday['date'] ?>">
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>

    </div>
</div>

</body>
</html>