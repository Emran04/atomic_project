<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Summary\Summary;

$ids = implode(',', $_POST['chk']);

$summary = new Summary();

$summary->recoverMultiple($ids);