<?php include_once '../../../includes/header.php' ?>
<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Create Books</h1>
        </div>

        <form action="store.php" method="post">
            <div class="form-group">
                <label for="book_title">Book Title</label>
                <input type="text" name="book_title" class="form-control" id="book_title" placeholder="Book Title">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Email">
            </div>
            <div class="form-group">
                <label>Description:</label>
                <textarea class="form-control" rows="5" id="mytextarea" name="description"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

    </div>
</div>
<?php include_once '../../../includes/footer.php' ?>
<script>
    tinymce.init({
        selector: '#mytextarea'
    });
</script>
</body>
</html>