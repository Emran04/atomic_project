<?php
namespace App\Bitm\SEIP137959\Message;

if(!isset($_SESSION['message'])) {
    session_start();
}

class Message
{
    public static function message($message = null, $lvl = "info")
    {
        if (is_null($message)) {
            return self::getMessage();
        } else {
            self::setMessage($message, $lvl);
        }
    }

    public static function getMessage()
    {
        $message['message'] = $_SESSION['message'];
        $message['lvl'] = $_SESSION['lvl'];
        $_SESSION['message'] = '';
        $_SESSION['lvl'] = '';
        return $message;
    }

    public static function setMessage($message, $lvl)
    {
        $_SESSION['message'] = $message;
        $_SESSION['lvl'] = $lvl;
    }
}