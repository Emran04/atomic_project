<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Hobby\Hobby;

$hobby = new Hobby();

$hobby->prepare($_GET)->recover();
