<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Summary\Summary;

$summary = new Summary();

$summarys = $summary->indexTrashed();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Organization Summary</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/css/style.css">
    <link rel="stylesheet" href="../../../resources/sweetalert/dist/sweetalert.css">
</head>
<body>


<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">
                HOME
            </a>
        </div>
    </div>
</nav>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Trashed Organizations</h1>
        </div>

        <form action="deletemultiple.php" method="post" id="tableAll">

            <div class="row">
                <div class="col-md-12">
                    <p>
                        <a href="create.php" class="btn btn-primary">Create New Organization</a>
                        <button type="submit" class="btn btn-success" id="recoverAll">Recover All</button>
                        <button type="submit" class="btn btn-danger" id="delAll">Delete All</button>
                    </p>
                </div>
            </div>


            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>
                        <label for="sAll">
                            <input type="checkbox" id="sAll"> Select All
                        </label>
                    </th>
                    <th>SL</th>
                    <th>ID</th>
                    <th class="tbl-content">Summary</th>
                    <th class="actions">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sl = 0;
                foreach($summarys as $summaryItem):
                    $sl++; ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="chk[]" value="<?= $summaryItem['id'] ?>">
                        </td>
                        <td><?= $sl ?></td>
                        <td><?= $summaryItem['id'] ?></td>
                        <td class="tbl-content"><?= $summaryItem['summary'] ?></td>
                        <td class="actions">
                            <a href="view.php?id=<?= $summaryItem['id'] ?>" class="btn btn-primary">View</a>
                            <a href="recover.php?id=<?= $summaryItem['id'] ?>" class="btn btn-success">Recover</a>
                            <a href="delete.php?id=<?= $summaryItem['id'] ?>" class="btn btn-danger dl">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </form>

    </div>

</div>

<?php include_once '../../../includes/footer.php' ?>

<script>
    $('#recoverAll').on("click", function (evt) {
        $('#tableAll').attr('action', 'recovermultiple.php').submit();
    });

    $('#delAll').on("click", function (evt) {
        evt.preventDefault();
        swal({
            title: "Are you sure?",
            text: "It will permanently delete All records!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete all!",
            cancelButtonText: "No, cancel !",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                $('#tableAll').attr('action', 'deletemultiple.php').submit();
            } else {
                return false;
            }
        });
    });

    $('#sAll').click(function(event) {
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });

</script>
</body>
</html>