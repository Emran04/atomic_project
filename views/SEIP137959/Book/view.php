<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Book\Book;

$book = new Book();

$singleBook = $book->prepare($_GET)->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Books</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/css/style.css">
</head>
<body>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Book</h1>
        </div>

        <ul class="list-group">
            <li class="list-group-item">ID: <?= $singleBook['id'] ?></li>
            <li class="list-group-item">Name: <?= $singleBook['book_title'] ?></li>
        </ul>

        <p>
            <a href="edit.php?id=<?= $singleBook['id'] ?>" class="btn btn-primary">Edit</a>
            <a href="delete.php?id=<?= $singleBook['id'] ?>" class="btn btn-danger">Delete</a>
        </p>

    </div>
</div>
</body>
</html>