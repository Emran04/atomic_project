<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;

if(isset($_FILES['image'])) {
    $image_name = time() . $_FILES['image']['name'];
    $image_temp_location = $_FILES['image']['tmp_name'];

    move_uploaded_file($image_temp_location, "../../../resources/images/" . $image_name);

    $_POST['image_name'] = $image_name;
}

$profile = new ImageUploader();

$profile->prepare($_POST)->store();