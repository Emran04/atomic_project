<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Book\Book;
$obj= new Book();
$allData=$obj->index();
//var_dump($allData);
$trs="";
$sl=0;
foreach($allData as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td> $sl</td>";
    $trs.="<td> {$data['id']}</td>";
    $trs.="<td> {$data['book_title']}</td>";
    $trs.="</tr>";
endforeach;
$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Book title</th>
              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>
BITM;

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('books.pdf', 'D');