<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\City\City;

$city = new City();

$city = $city->prepare($_GET)->view();
$cities = explode(', ', $city['city']);

?>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <h1>Select City</h1>

        <p>City: <?= $city['city'] ?></p>

        <form action="update.php" method="post">
            <input type="hidden" name="id" value="<?= $city['id'] ?>">
            <div class="form-group">
                <select class="form-control" name="city[]" multiple>
                    <option disabled> -- select a city -- </option>
                    <option <?php if( in_array("Chittagong", $cities) ) echo "selected"; ?> value="Chittagong">Chittagong</option>
                    <option <?php if( in_array("Dhaka", $cities) ) echo "selected"; ?> value="Dhaka">Dhaka</option>
                    <option <?php if( in_array("Rangpur", $cities) ) echo "selected"; ?>>Rangpur</option>
                    <option <?php if( in_array("Sylhet", $cities) ) echo "selected"; ?> value="Sylhet">Sylhet</option>
                    <option <?php if( in_array("Commilla", $cities) ) echo "selected"; ?> value="Commilla">Commilla</option>
                </select>
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>

    </div>
</div>

</body>
</html>