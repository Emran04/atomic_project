<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;
use App\Bitm\SEIP137959\Message\Message;

$profile = new ImageUploader();

$profiles = $profile->indexTrash();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profiles</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/css/style.css">
    <link rel="stylesheet" href="../../../resources/sweetalert/dist/sweetalert.css">
</head>
<body>

<nav>
    <div class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a href="index.php" class="navbar-brand">HOME</a>
            </div>
        </div>
    </div>
</nav>

<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <h1>Trashed Profiles</h1>

        <form action="deletemultiple.php" method="post" id="fAll">
            <p>
                <a href="create.php" class="btn btn-primary">Create New Profile</a>
                <button type="submit" class="btn btn-success" id="recoverAll">Recover All</button>
                <button type="submit" class="btn btn-danger" id="delAll">Delete All</button>
            </p>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        <label><input type="checkbox" id="sAll"> Select All</label>
                    </th>
                    <th>SL</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Profile</th>
                    <th class="actions">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sl = 0;
                foreach($profiles as $profile):
                    $sl++; ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="chk[]" value="<?= $profile['id'] ?>">
                        </td>
                        <td><?= $sl ?></td>
                        <td><?= $profile['id'] ?></td>
                        <td><?= $profile['name'] ?></td>
                        <td><img src="../../../resources/images/<?= $profile['image_name'] ?>" alt="Image" height="150px" class="img-rounded"></td>
                        <td class="actions">
                            <a href="view.php?id=<?= $profile['id'] ?>" class="btn btn-primary">View</a>
                            <a href="recover.php?id=<?= $profile['id'] ?>" class="btn btn-success">Recover</a>
                            <a href="delete.php?id=<?= $profile['id'] ?>" class="btn btn-danger dl">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </form>

    </div>
</div>

<?php include_once '../../../includes/footer.php' ?>

<script>
    $('#recoverAll').on("click", function () {
        $('#fAll').attr('action', 'recovermultiple.php').submit();
    });

    $('#delAll').on("click", function (evt) {
        evt.preventDefault();
        swal({
            title: "Are you sure?",
            text: "It will permanently delete All records!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete all!",
            cancelButtonText: "No, cancel !",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                $('#fAll').attr('action', 'deletemultiple.php').submit();
            } else {
                return false;
            }
        });
    });

    $('#sAll').click(function() {
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });

</script>
</body>
</html>