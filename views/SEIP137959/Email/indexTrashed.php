<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Email\Email;

$email = new Email();

$emailList = $email->indexTrashed();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Emails</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/css/style.css">
    <link rel="stylesheet" href="../../../resources/sweetalert/dist/sweetalert.css">
</head>
<body>


<nav>
    <div class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a href="index.php" class="navbar-brand">HOME</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="indexTrashed.php">TRASHED</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <div class="page-header">
            <h1>Trashed Emails</h1>
        </div>

        <form action="deletemultiple.php" method="post" id="fAll">
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <a href="create.php" class="btn btn-primary">Create New Email</a>
                        <button type="submit" class="btn btn-success" id="recoverAll">Recover All</button>
                        <button type="submit" class="btn btn-danger" id="delAll">Delete All</button>
                    </p>
                </div>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        <label><input type="checkbox" id="sAll"> Select All</label>
                    </th>
                    <th>SL</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="actions">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sl = 0;
                foreach($emailList as $emailItem):
                    $sl++; ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="chk[]" value="<?= $emailItem['id'] ?>">
                        </td>
                        <td><?= $sl  ?></td>
                        <td><?= $emailItem['id'] ?></td>
                        <td><?= $emailItem['email'] ?></td>
                        <td class="actions">
                            <a href="view.php?id=<?= $emailItem['id'] ?>" class="btn btn-primary">View</a>
                            <a href="recover.php?id=<?= $emailItem['id'] ?>" class="btn btn-success">Recover</a>
                            <a href="delete.php?id=<?= $emailItem['id'] ?>" class="btn btn-danger dl">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </form>



    </div>
</div>

<?php include_once '../../../includes/footer.php' ?>

<script>
    $('#recoverAll').on("click", function () {
        $('#fAll').attr('action', 'recovermultiple.php').submit();
    });

    $('#sAll').click(function() {
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });

    $('#delAll').on("click", function (evt) {
        evt.preventDefault();
        swal({
            title: "Are you sure?",
            text: "It will permanently delete All records!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete all!",
            cancelButtonText: "No, cancel !",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                $('#fAll').attr('action', 'deletemultiple.php').submit();
            } else {
                return false;
            }
        });
    });
</script>
</body>
</html>