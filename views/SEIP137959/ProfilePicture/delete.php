<?php
require_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;

$profile = new ImageUploader();

$profiles = $profile->prepare($_GET)->view();


$path = $_SERVER['DOCUMENT_ROOT'] . "/AtomicProjects/resources/images/" . $profiles['image_name'];

unlink($path);

$profile->delete();