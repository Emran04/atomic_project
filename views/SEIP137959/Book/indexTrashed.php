<?php
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Book\Book;

$book = new Book();

$bookList = $book->indexTrashed();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Books</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/css/style.css">
    <link rel="stylesheet" href="../../../resources/sweetalert/dist/sweetalert.css">
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">
                HOME
            </a>
        </div>
    </div>
</nav>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Trashed Books</h1>
        </div>

        <form action="recovermultiple.php" method="post" id="multiple">
            <p>
                <a href="create.php" class="btn btn-primary">Create New Book</a>
                <button type="submit" class="btn btn-success">Recover Selected</button>
                <button type="submit" id="deleteMultiple" class="btn btn-danger">Delete All Selected</button>
            </p>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        <label><input type="checkbox" id="sAll"> Select All</label>
                    </th>
                    <th>SL</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th class="actions">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sl = 0;
                foreach($bookList as $bookItem):
                    $sl++; ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="mark[]" value="<?= $bookItem['id'] ?>">
                        </td>
                        <td><?= $sl ?></td>
                        <td><?= $bookItem['id'] ?></td>
                        <td><?= $bookItem['book_title'] ?></td>
                        <td class="actions">
                            <a href="view.php?id=<?= $bookItem['id'] ?>" class="btn btn-primary">View</a>
                            <a href="recover.php?id=<?= $bookItem['id'] ?>" class="btn btn-success">Recover</a>
                            <a href="delete.php?id=<?= $bookItem['id'] ?>" class="btn btn-danger dl">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </form>

    </div>
</div>

<?php include_once '../../../includes/footer.php' ?>

<script>

    $('#deleteMultiple').on("click", function (evt) {
        evt.preventDefault();
        swal({
            title: "Are you sure?",
            text: "It will permanently delete All records!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete all!",
            cancelButtonText: "No, cancel !",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                $('#multiple').attr('action', 'deletemultiple.php').submit();
            } else {
                return false;
            }
        });
    });

    $('#sAll').click(function() {
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });

</script>
</body>
</html>