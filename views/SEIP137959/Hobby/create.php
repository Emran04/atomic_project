<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h2>Select hobbies</h2>
        </div>
        <form role="form" action="store.php" method="post">
            <div class="input-group">
                <label><input type="checkbox" name="mark[]" value="Football"> Playing Football</label>
            </div>
            <div class="input-group">
                <label><input type="checkbox" name="mark[]" value="Hiking"> Hiking</label>
            </div>
            <div class="input-group">
                <label><input type="checkbox" name="mark[]" value="Travelling"> Travelling</label>
            </div>
            <div class="input-group">
                <label><input type="checkbox" name="mark[]" value="Fishing"> Fishing</label>
            </div>
            <div class="input-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </div>
</div>

</body>
</html>

