<?php
require_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;

$profile = new ImageUploader();

$ids = implode(',', $_POST['chk']);


$profile->recoverMultiple($ids);