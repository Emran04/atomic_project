<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\ProfilePicture\ImageUploader;

$profile = new ImageUploader();

$profiles = $profile->prepare($_GET)->view();

?>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <div class="page-header">
            <h1>Update Profile</h1>
        </div>

        <form action="update.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $profiles['id'] ?>">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" value="<?= $profiles['name'] ?>">
            </div>
            <div class="panel">
                <p>Profile Picture</p>
                <img src="../../../resources/images/<?= $profiles['image_name'] ?>" alt="Image" height="150px">
            </div>
            <div class="form-group">
                <label for="title">Name</label>
                <input type="file" name="image" class="form-control" id="title">
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>

    </div>
</div>
</body>
</html>