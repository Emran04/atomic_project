<?php
require_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\City\City;

$ids = implode(",", $_POST['mark']);

$city = new City();

$city->deleteMultiple($ids);
