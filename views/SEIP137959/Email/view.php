<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Email\Email;

$email = new Email();

$emailSingle = $email->prepare($_GET)->view();
?>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Email</h1>
        </div>

        <div class="list-group">
            <div class="list-group-item">ID: <?= $emailSingle['id'] ?></div>
            <div class="list-group-item">Email: <?= $emailSingle['email'] ?></div>
        </div>

        <p>
            <a href="edit.php?id=<?= $emailSingle['id'] ?>" class="btn btn-primary">Edit</a>
            <a href="delete.php?id=<?= $emailSingle['id'] ?>" class="btn btn-danger">Delete</a>
        </p>

    </div>
</div>

</body>
</html>
