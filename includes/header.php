<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Project</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/css/style.css">
    <link rel="stylesheet" href="../../../resources/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

</head>
<body>

<nav>
    <div class="navbar navbar-default mainmenu">
        <div class="container">
            <ul class="nav navbar-nav">
                <li><a href="index.php" class="navbar-brand">HOME</a></li>
                <li><a href="indexTrashed.php" class="navbar-brand">TRASHED</a></li>
            </ul>
            <a href="http://localhost/atomicprojects" class="navbar-brand pull-right">All</a>
        </div>
    </div>
</nav>
