<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Email\Email;

$email = new Email();

$emailSingle = $email->prepare($_GET)->view();
?>

<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Email</h1>
        </div>

        <form action="update.php" method="post">
            <input type="hidden"name="id" value="<?=$emailSingle['id']?>">
            <div class="form-group">
                <label for="title">Enter Name</label>
                <input type="text" name="name" class="form-control" id="title" value="<?=$emailSingle['name']?>">
            </div>
            <div class="form-group">
                <label for="email">Enter Email</label>
                <input type="email" name="email" class="form-control" id="email" value="<?=$emailSingle['email']?>">
            </div>
            <button type="submit" class="btn btn-success">Update</button>
        </form>

    </div>
</div>
</body>
</html>