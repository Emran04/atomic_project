<?php

namespace App\Bitm\SEIP137959\Summary;

use App\Bitm\SEIP137959\Message\Message;

class Summary
{
    public $id = '',
        $name = '',
        $summary = '',
        $conn,
        $deleted_at;

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'atomicprojects');
    }

    public function prepare($data = '')
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        if(array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

        if(array_key_exists('summary', $data)) {
            $this->summary = $data['summary'];
        }

        return $this;
    }

    public function index()
    {
        $_all = array();
        $query = "SELECT * FROM `summarys` WHERE deleted_at IS NULL";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_all[] = $row;
        }

        return $_all;
    }

    public function indexTrashed()
    {
        $_allRslts = array();
        $query = "SELECT * FROM `summarys` WHERE deleted_at IS NOT NULL";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }


    public function store()
    {
        $query = "INSERT INTO `summarys` (`name`, `summary`) VALUES ('".$this->name."', '".$this->summary."')";
        if( mysqli_query($this->conn, $query)) {
            Message::message("Data successfully stored", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        $query = "UPDATE `summarys` SET `name` = '". $this->name ."', `summary` = '". $this->summary ."' WHERE `id` = ". $this->id;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Updated", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function view()
    {
        $query = "SELECT * FROM `summarys` WHERE `id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    public function delete()
    {
        $query = "DELETE FROM `summarys` WHERE `id` = " .$this->id;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully Deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }

    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `summarys` SET `deleted_at` = '". $this->deleted_at ."' WHERE `id` = ".$this->id ;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully trashed", "warning");
            header('Location: index.php');
        } else {
            echo "Error";
        }

    }

    public function recover()
    {
        $query = "UPDATE `summarys` SET `deleted_at` = NULL WHERE `id` = ".$this->id ;
        if (mysqli_query($this->conn, $query)) {
            Message::message("Data successfully recovered", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function count()
    {
        $query = "SELECT count(*) AS totalItems FROM `summarys` WHERE deleted_at IS NULL";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItems'];

    }

    public function paginator($start, $limit)
    {
        $_allRslts = [];
        $query = "SELECT * FROM `summarys` WHERE `deleted_at` IS NULL LIMIT {$start},{$limit}";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }

    public function recoverMultiple($ids)
    {
        $query = "UPDATE `summarys` SET `deleted_at` = NULL WHERE `id` IN ({$ids})";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All data successfully recovered", "success");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }

    public function deleteMultiple($ids)
    {
        $query = "DELETE FROM `summarys` WHERE `id` IN({$ids})";
        if (mysqli_query($this->conn, $query)) {
            Message::message("All data successfully deleted", "danger");
            header('Location: index.php');
        } else {
            echo "Error";
        }
    }
}