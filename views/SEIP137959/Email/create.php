<?php include_once '../../../includes/header.php' ?>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Email</h1>
        </div>

        <form action="store.php" method="post">
            <div class="form-group">
                <label for="title">Enter Name</label>
                <input type="text" name="name" class="form-control" id="title" placeholder="Name">
            </div>
            <div class="form-group">
                <label for="email">Enter Email</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Email">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

    </div>
</div>
</body>
</html>