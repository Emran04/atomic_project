<?php
session_start();
include_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Email\Email;
$obj= new Email();
$allData=$obj->index();
$trs="";
$sl=0;
foreach($allData as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td> $sl</td>";
    $trs.="<td> {$data['id']}</td>";
    $trs.="<td> {$data['name']}</td>";
    $trs.="<td> {$data['email']}</td>";
    $trs.="</tr>";
endforeach;
$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>
BITM;

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('mail.pdf', 'D');