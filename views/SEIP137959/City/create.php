<?php include_once '../../../includes/header.php' ?>
<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h1>Select City</h1>
        </div>

        <form action="store.php" method="post">
            <div class="form-group">
                <select class="form-control" name="city[]" multiple>
                    <option disabled selected value> -- select a city -- </option>
                    <option value="Chittagong">Chittagong</option>
                    <option value="Dhaka">Dhaka</option>
                    <option value="Rangpur">Rangpur</option>
                    <option value="Sylhet">Sylhet</option>
                    <option value="Commilla">Commilla</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

    </div>
</div>

</body>
</html>