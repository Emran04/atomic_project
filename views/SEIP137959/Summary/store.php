<?php
require_once '../../../vendor/autoload.php';
use App\Bitm\SEIP137959\Summary\Summary;


$summary = new Summary();

$summary->prepare($_POST)->store();